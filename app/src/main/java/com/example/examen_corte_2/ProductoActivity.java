package com.example.examen_corte_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examen_corte_2.database.Producto;
import com.example.examen_corte_2.database.ProductosDB;

public class ProductoActivity extends AppCompatActivity {
    private EditText txtCodigo;
    private Button btnBuscar;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup rbgPerecedero;
    private Button btnBorrar;
    private Button btnEditar;
    private Button btnCerrar;
    private ProductosDB db;
    private Producto saveObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbgPerecedero = findViewById(R.id.rbgPerecedero);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnEditar = findViewById(R.id.btnEditar);
        btnCerrar = findViewById(R.id.btnCerrar);
        db = new ProductosDB(this);

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if(validar())
                {
                    Producto producto = new Producto();
                    producto.setId(saveObject.getId());
                    producto.setNombreProducto(getText(txtNombre));
                    producto.setPreacio(Float.parseFloat(getText(txtPrecio)));
                    producto.setMarca(getText(txtMarca));
                    producto.setPerecedero(rbgPerecedero.getCheckedRadioButtonId() == R.id.rbPerecedero);
                    if (db.updateProducto(producto) != -1)
                        Toast.makeText(ProductoActivity.this, "Se actualizo correctamente", Toast.LENGTH_SHORT ).show();
                    else
                        Toast.makeText(ProductoActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(ProductoActivity.this, "No puedes dejar los campos vacios", Toast.LENGTH_SHORT).show();

                }
                limpiar();
                db.close();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                Producto producto = db.getProducto(Long.parseLong(getText(txtCodigo)));
                if(producto == null)
                {
                    Toast.makeText(ProductoActivity.this, "No se encontro producto", Toast.LENGTH_LONG).show();
                }
                else
                {
                    saveObject = producto;
                    txtMarca.setText(saveObject.getMarca());
                    txtNombre.setText(saveObject.getNombreProducto());
                    txtPrecio.setText(String.valueOf(saveObject.getPreacio()));
                    rbgPerecedero.check(saveObject.isPerecedero() ? R.id.rbPerecedero : R.id.rbNoPerecedero);
                }
                db.close();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                db.deleteProducto(Long.parseLong(getText(txtCodigo)));
                Toast.makeText(ProductoActivity.this, "Se elimino el producto", Toast.LENGTH_LONG).show();
                db.close();
                limpiar();
            }
        });
    }

    private void limpiar()
    {
        txtCodigo.setText("");
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        saveObject = null;

    }
    private boolean validar(){
        if(validarCampo(txtCodigo) || validarCampo(txtNombre) ||
                validarCampo(txtPrecio) || validarCampo(txtMarca))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
}
