package com.example.examen_corte_2.database;

public abstract class DefinirTabla {
    public final static String ID = "id";
    public final static String CODIGO = "codigo";
    public final static String NOMBRE_PRODUCTO = "nombre";
    public final static String MARCA = "marca";
    public final static String PRECIO = "precio";
    public final static String PERECEDERO = "perecedero";
    public final static String TABLE_PRODUCTO = "producto";

}
